class AddPublishedToBlorghArticles < ActiveRecord::Migration[6.0]
  def change
    add_column :blorgh_articles, :published, :boolean
  end
end
