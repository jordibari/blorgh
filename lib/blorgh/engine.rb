require 'jquery-rails'

module Blorgh
  class Engine < ::Rails::Engine
    isolate_namespace Blorgh
    initializer :assets do |config|
      Rails.application.config.assets.paths << root.join("app", "assets", "images", "blorgh")
    end
  end
end
